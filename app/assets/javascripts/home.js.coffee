window.Freeagent ||= {}

class Freeagent.CurrencyExchangeModel
  FREEAGENT_API_URL: "http://localhost:3000/api/v1/exchange_rates"
   
  constructor: ->
    @date = ko.observable()
    @from_currency = ko.observable()
    @to_currency = ko.observable()
    @amount = ko.observable()
    @status = ko.observable()
    @result = ko.observable()

  loadingStatus: ->
    @status() != ""

  statusColor: ->
    if @status() == ""
      ""
    else if @status() == "Calculating..."
      "alert-warning"
    else if @status() == "Complete"
      "alert-success"
    else
      "alert-danger"

  displayResult: (result) ->
    @result result

  calculateResult: (exchange_rate) ->
    result = (@amount() * exchange_rate).toFixed(2)
    @displayResult result

  loadExchangeRate: (form_element) =>
    @clearResult()
    @status "Calculating..."
    form_data = { date: @date(), from_currency: @from_currency(), to_currency: @to_currency(), amount: @amount() }

    $.getJSON(@FREEAGENT_API_URL, form_data, (data) =>
      if data.success == true
        @calculateResult data.result
        @status "Complete"
      else
        @status "Error: " + data.result

    ).fail (data) =>
      @status "Error: " + data.responseJSON.error

  clearResult: ->
    @result ""

class Freeagent.Home extends Freeagent.Base
  constructor: ->
    super
    this

  index:() ->
    ko.applyBindings new Freeagent.CurrencyExchangeModel
