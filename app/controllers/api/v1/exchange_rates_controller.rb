module Api
  module V1
    class ExchangeRatesController < ApplicationController
      REQUIRED_PARAMS = [:date, :from_currency, :to_currency, :amount]

      before_filter :validate_params

      def index
        begin
          exchange_rate = Exchangerate.at(params[:date], params[:from_currency].to_s.upcase, params[:to_currency].to_s.upcase)
          render :json => {success: true, result: exchange_rate}
        rescue Exchangerate::ParamsError => e
          render :json => {success: false, result: e.message}
        end
      end

      protected

      def validate_params
        if (REQUIRED_PARAMS - params.symbolize_keys.keys).count > 0
          render :json => {success: false, result: "Missing parameters, they must include #{REQUIRED_PARAMS.join(", ")}"}
        elsif (Float(params[:amount]) == nil rescue true)
          render :json => {success: false, result: "Amount must be a number"}
        end
      end

    end
  end
end
