module Home
  class IndexPage
    include PageObject, DataMagic, Common::UrlHelper
  
    page_url build_uri '/'
  
    div :header, :class => "navbar-fixed-top"
    div :jumbotron, :class => "jumbotron"
    div :entry_form, :class => "entry-form"
    div :result, :class => "result"
    div :alert, :class => "alert"
    text_field :date, :id => "date"
    text_field :from_currency, :id => "from_currency"
    text_field :to_currency, :id => "to_currency"
    text_field :amount, :id => "amount"
    button :submit, :name  => "submit"
  
    def calculate(date, from_currency, to_currency, amount)
      self.date = date
      self.from_currency = from_currency
      self.to_currency = to_currency
      self.amount = amount
      self.submit
    end
  
    def render_successfull?
      header_exists? &&
        jumbotron_exists? &&
        entry_form_exists? &&
        result_exists?
    end
  
    def header_exists?
      !self.header.nil?
    end
  
    def jumbotron_exists?
      !self.jumbotron.nil?
    end
  
    def entry_form_exists?
      !self.entry_form.nil?
    end
  
    def result_exists?
      !self.result.nil?
    end
  
  end
end
