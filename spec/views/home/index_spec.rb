require 'spec_helper'

describe "Test Freeagent currency convertor page" do

  describe "Check layout of index page" do
    context "visit index page" do
      it "should have header with title, jumbobox, entry form box and results box" do
        visit(Home::IndexPage) do |page|
          expect(page.render_successfull?).to eq true
        end
      end
    end

    context "perform currency conversion" do
      it "Should return valid conversion" do
        visit(Home::IndexPage) do |page|
          page.calculate "2015-09-23", "USD", "GBP", "100"
          page.wait_until(30, "API call not returned within 30 seconds") do
            page.text.include? "Complete"
          end
        end
      end

      it "Should return invalid conversion" do
        visit(Home::IndexPage) do |page|
          page.calculate "2015-09-23", "XYZ", "ABC", "100"
          page.wait_until(30, "API call not returned within 30 seconds") do
            page.text.include? "Error"
          end
        end
      end
    end

  end
end
