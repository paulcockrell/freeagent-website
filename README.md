# Freeagent exchange rate calculator

Single page website to calculate exchange rate and apply to a given amount. Uses ECB exchange rates.

Uses Exchangerate gem, which is found at git@bitbucket.org:paulcockrell/freeagent-exchange-rate.git

the Exchangerate gem is configure in config/initializers/exchangerate.rb, it uses the ECB exchange
rates file downloaded to tmp/exchange-rates/development/eurofxref-hist-90d.xml

Install and run
---------------

1. Install Ruby 2.1.5 and bundle gem
2. Install webapp

```
$> git clone https://github.com/paulcockrell/freeagent-website.git
$> cd ./freeagent-website
$> bundle install
$> gem install libv8 -v "3.16.14.3" -- --with-system-v8 && bundle install # if on Mac and you have troubles installing
```

3. Run webapp

```
$> bundle exec rails s -p 3000
```
4. Visit http://localhost:3000 in your browser
5. Enter date, from currency, to currency, amount and click load button, you will get a response after a short while.

Test
----
The tests for the webapp are Browser tests using Watir (which uses Selenium)
You must have the WEB app running on localhost, port 3000 in the background, or in another shell to run these test.

```
$> cd /path/to/freeagent-website
$> bundle exec rspec -f documentation
```
