Exchangerate.configure do |config|
  config.source = Rails.root.join("tmp","exchange-rates", Rails.env, "eurofxref-hist-90d.xml").to_s
end
